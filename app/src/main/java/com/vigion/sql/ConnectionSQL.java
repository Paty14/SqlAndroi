package com.vigion.sql;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.test.suitebuilder.annotation.Suppress;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by a45321 on 07-01-2016.
 */
public class ConnectionSQL {

    //SQLServer
    //private String server = "10.1.26.66";         //ip ou url da maquina com oSQLServer         ESCOLA
    private String server = "10.1.16.183";         //ip ou url da maquina com oSQLServer         CASA
    private String driverSQLServer ="net.sourceforge.jtds.jdbc.Driver";     //Connector
    private String database = "Escola";
    private String dbUserId = "tgpsi";
    private String dbPassword = "esferreira123";

    //MySQL
    private String mySqlDriver = "com,mysql.jdbc.Driver";
    private String mySqlServerLocation = "192.168.56.1";
    //private String mySqlServerLocation = "http://localhost/phpmyadmin/sql.php?db=escola&table=aluno&token=1b53cc5ec772691b8638b348f167b2a3&pos=0";
    private String mySqlDatabase = "escola";
    private String mySqlDbUserId = "root";
    private String mySqlDbPassword = "";

    //Método SQLServer
    //Método devolve uma Connection Pronta a usar
    @SuppressLint("NewApi") //suprime varnnings desta classe
    public Connection connectSQLServer(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try{
            Class.forName(driverSQLServer);
            ConnURL = "jdbc:jtds:sqlserver://" + server
                    + ";databaseName=" + database
                    + ";user=" + dbUserId
                    + ";password=" + dbPassword + ";";

            conn = DriverManager.getConnection(ConnURL);
        }
        catch (SQLException ex){
            Log.e("ERRO", ex.getMessage());
        }
        catch (ClassNotFoundException ex){
            Log.e("ERRO", ex.getMessage());
        }
        catch (Exception ex){
            Log.e("ERRO", ex.getMessage());
        }
        return conn;
    }

    //Método MySQL
    @SuppressLint("NewApi") //suprime varnnings desta classe
    public Connection connectMySql(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try{
            Class.forName(mySqlDriver);
            ConnURL = "jdbc:mysql://" + mySqlServerLocation
                    + ";databaseName=" + mySqlDatabase
                    + ";user=" + mySqlDbUserId
                    + ";password=" + mySqlDbPassword + ";";
            conn = DriverManager.getConnection(ConnURL);
        }
        catch (SQLException ex){
            Log.e("ERRO", ex.getMessage());
        }
        catch (ClassNotFoundException ex){
            Log.e("ERRO", ex.getMessage());
        }
        catch (Exception ex){
            Log.e("ERRO", ex.getMessage());
        }
        return conn;
    }
}
